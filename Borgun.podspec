#
# Be sure to run `pod lib lint Borgun.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Borgun'
  s.version          = '1.0'
  s.summary          = 'Borgun payment library (CocoaPod)'
  s.description      = 'This is a library to handle payements through Borgun'
  s.homepage         = 'https://bitbucket.org/stokkur/borgun-cocoapod-repo'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Stokkur' => 'stokkur@stokkur.is' }
  s.source           = { :git => 'https://bitbucket.org/stokkur/borgun-cocoapod-repo.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'Borgun/*'
  
   s.resource_bundles = {
     'Borgun' => ['Borgun/ResourceBundle.bundle']
   }
end
