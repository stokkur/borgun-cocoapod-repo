//
//  BP_Merchant_API.h
//  BorgunPaymentHandler
//
//  Created by Alok Sahay on 1/13/16.
//  Copyright © 2016 Stokkur s.r.o. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PaymentViewController.h"

@interface PaymentSubscriptionAPI : NSObject

typedef enum :NSInteger {
    MerchantRequestCancel,
    MerchantRequestRefund,
    MerchantRequestCapture
} MerchantRequest;


+ (void)makeRequest:(MerchantRequest)request scheme:(UrlScheme)scheme authToken:(NSString *)authToken forTransactionId:(NSString*)transactionId
                                            WithBlockSuccess:(void(^)(id responseObject))blockSuccess
                                                     failure:(void(^)(id responseObject))blockFailure;


@end
