//
//  BaseViewController.m
//  BorgunSampleProject
//
//  Created by Alok Sahay on 12/21/15.
//  Copyright © 2015 Stokkur s.r.o. All rights reserved.
//

#import "BaseViewController.h"

//include this header to access the library
#import <Borgun/PaymentViewController.h>

@interface BaseViewController ()

////////// Properties //////////
@property (weak, nonatomic) IBOutlet UISegmentedControl *segment_theme;
@property (weak, nonatomic) IBOutlet UISlider *slider_amount;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segment_cards;
@property (weak, nonatomic) IBOutlet UILabel *lbl_amount;
////////////////////////////////

@end

@implementation BaseViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self amount_changed:self];
}


#pragma mark Payment Controller

- (IBAction)showpvc:(id)sender {
    
    //create our controller
    PaymentViewController *paymentController = [[PaymentViewController alloc] init];
    
    //what card types do you want to provide
    AUCreditCardType cardsAllowed;
    
    switch (_segment_cards.selectedSegmentIndex) {
        case 0: // Visa and Electron
            cardsAllowed = AUCreditCardTypeVisa | AUCreditCardTypeVisaElectron;
            break;
        case 1: // Mastercard and Maestro
            cardsAllowed = AUCreditCardTypeMasterCard | AUCreditCardTypeMaestro;
            break;
        case 2: // All cards
            cardsAllowed = AUCreditCardTypeAll;
            break;
            
        default:// Default
            cardsAllowed = AUCreditCardTypeNone;
            break;
    }
    
    //some example amount
    int amount = (int)(_slider_amount.value * 100);
    
    //authentication token example
    NSString* authToken = @"the autorization token";
    
    //order ID example
    NSString* orderId = @"the order id for the following payment";
    
    //user example
    NSString* user = @"the user id for the following payment";
    
    //example currency
    Currency currency = CurrencyEUR;
    
    //the payment method
    PaymentMethod paymentMethod = PaymentMethodPreAuth;
    
    //the environment e.g. dev/prod
    UrlScheme scheme = UrlSchemeDev;
    
    //setup the controller
    [paymentController setupWithAuthToken:authToken scheme:scheme amount:amount paymentMethod:paymentMethod currencyCode:currency andOrderId:orderId forUser:user allowedCards:cardsAllowed withBlockResult:^(BOOL result, id responseObject) {
        
        //dismissed from cancel
        if (!result) {
            [paymentController dismissViewControllerAnimated:YES completion:nil];
        }
        
    } withCompletion:^{
        
        //payment success
        [paymentController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    //set the theme light/dark
    paymentController.theme = DisclaimerViewThemeDark;
    //set the background image
    paymentController.backgroundImage = [self imageForTheme:(int)_segment_theme.selectedSegmentIndex];
    //set the backgroundColor
    paymentController.backgroundColor = [self bgColorForTheme:(int)_segment_theme.selectedSegmentIndex];
    //set the background color for the user input fields
    paymentController.colorSectionBackground = [self fieldBgColorForTheme:(int)_segment_theme.selectedSegmentIndex];
    //set the text color for the user input fields
    paymentController.colorTextInputs = [self textColorForTheme:(int)_segment_theme.selectedSegmentIndex];
    //set the text color for the user input names
    paymentController.colorAccents = [self tintColorForTheme:(int)_segment_theme.selectedSegmentIndex];
    
    //presenting modally is supported
    [self presentViewController:paymentController animated:YES completion:nil];
}


- (IBAction)amount_changed:(id)sender {
    
    _lbl_amount.text = [NSString stringWithFormat:@"%d", (int)(_slider_amount.value * 100)];
}


#pragma mark Theme examples

-(UIImage *)imageForTheme:(int)theme {
    
    switch (theme) {
        case 0:
            return  [UIImage imageNamed:@"eve.jpg"];
            break;
        case 1:
            return  [UIImage imageNamed:@"wapp.jpg"];
            break;
        default:
            return nil;
            break;
    }
}


-(UIColor *)bgColorForTheme:(int)theme {
    
    switch (theme) {
        case 2:
            return  [UIColor whiteColor];
            break;
        case 3: {
            int hexCode = 0xd04f3f;
            return  [UIColor colorWithRed:((float)((hexCode & 0xFF0000) >> 16))/255.0 green:((float)((hexCode & 0xFF00) >> 8))/255.0 blue:((float)(hexCode & 0xFF))/255.0 alpha:1.0];
        }
            break;
        default:
            return [UIColor clearColor];
            break;
    }
}


-(UIColor *)textColorForTheme:(int)theme {
    
    switch (theme) {
        case 0:
        case 1:
        case 3:
            return  [UIColor whiteColor];
            break;
        default:
            return [UIColor blackColor];
            break;
    }
}


-(UIColor *)tintColorForTheme:(int)theme {
    
    switch (theme) {
        case 0:
        case 2:
            return  [UIColor orangeColor];
            break;
        case 1: {
            int hexCode = 0x6a92c3;
            return  [UIColor colorWithRed:((float)((hexCode & 0xFF0000) >> 16))/255.0 green:((float)((hexCode & 0xFF00) >> 8))/255.0 blue:((float)(hexCode & 0xFF))/255.0 alpha:1.0];
        }
            break;
            
        case 3: {
            int hexCode = 0xd28278;
            return  [UIColor colorWithRed:((float)((hexCode & 0xFF0000) >> 16))/255.0 green:((float)((hexCode & 0xFF00) >> 8))/255.0 blue:((float)(hexCode & 0xFF))/255.0 alpha:1.0];
        }
            break;
        default:
            return [UIColor blackColor];
            break;
    }
}


-(UIColor *)fieldBgColorForTheme:(int)theme {
    
    switch (theme) {
            
        case 2:{
            return  [UIColor whiteColor];
            break;
        }
        default:
            return [UIColor blackColor];
            break;
    }
}

@end
