//
//  main.m
//  BorgunSampleProject
//
//  Created by Alok Sahay on 12/21/15.
//  Copyright © 2015 Stokkur s.r.o. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
