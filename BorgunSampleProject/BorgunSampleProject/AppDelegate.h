//
//  AppDelegate.h
//  BorgunSampleProject
//
//  Created by Alok Sahay on 12/21/15.
//  Copyright © 2015 Stokkur s.r.o. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

