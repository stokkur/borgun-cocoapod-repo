//
//  PaymentViewController.h
//  BorgunPaymentLib
//
//  Created by Alok Sahay on 12/18/15.
//  Copyright © 2015 Stokkur s.r.o. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PaymentWrapperView;

/**
 * The languages available.
 */
typedef enum : NSUInteger {
    PaymentLanguageEnglish,
    PaymentLanguageCzech,
    PaymentLanguageIcelandic
} PaymentLanguage;


/**
 * The themes available. Light or Dark.
 */
typedef enum : NSUInteger {
    DisclaimerViewThemeLight,
    DisclaimerViewThemeDark
} DisclaimerViewTheme;


/**
 * The currencies that are available in the library.
 */
typedef enum : NSUInteger {
    CurrencyISK, //Icelandic Krona
    CurrencyCZK, //Czech Republic Koruna
    CurrencyEUR, //Euro
    CurrencyUSD, //US Dollar
    CurrencyGBP, //British Pound
    CurrencyDKK, //Danish Krone
    CurrencyNOK, //Norwegian Krone
    CurrencySEK, //Swedish Krona
    CurrencyCHF, //Swiss Franc
    CurrencyCAD, //Canadian Dollar
    CurrencyHUF, //Hungarian Forint
    CurrencyBHD, //Bahraini Dinar
    CurrencyAUD, //Australian Dollar
    CurrencyRUB, //Russian Ruble
    CurrencyPLN, //Polish Zloty
    CurrencyRON, //Romanian Leu
    CurrencyHRK, //Croatian Kuna
} Currency;

/**
 * The payment method used.
 */
typedef enum : NSUInteger {
    PaymentMethodPreAuth,
    PaymentMethodSale
} PaymentMethod;


/**
 * The active environment. Develop and Production.
 */
typedef enum : NSUInteger {
    UrlSchemeDev,
    UrlSchemeProduction
} UrlScheme;


/**
 * The types of cards that are available.
 */
typedef enum : NSUInteger {
    AUCreditCardTypeNone            = 0,
    AUCreditCardTypeVisaElectron    = (1 << 0),
    AUCreditCardTypeVisa            = (1 << 1),
    AUCreditCardTypeMasterCard      = (1 << 2),
    AUCreditCardTypeMaestro         = (1 << 3),
    AUCreditCardTypeAmex            = (1 << 4),
    AUCreditCardTypeDiscover        = (1 << 5),
    AUCreditCardTypeDiners          = (1 << 6),
    AUCreditCardTypeUnionPay        = (1 << 7),
    AUCreditCardTypeJCB             = (1 << 8),

    AUCreditCardTypeAll             = (AUCreditCardTypeVisaElectron| AUCreditCardTypeVisa | AUCreditCardTypeMaestro | AUCreditCardTypeDiscover | AUCreditCardTypeMasterCard | AUCreditCardTypeAmex | AUCreditCardTypeDiners | AUCreditCardTypeJCB | AUCreditCardTypeUnionPay),
    AUCreditCardTypeCount           = 9
} AUCreditCardType;

@interface PaymentViewController : UIViewController

/**
 * Setup the payment.
 * @param authToken The authentication token.
 * @param scheme The current scheme selected.
 * @param amount The amount selected.
 * @param paymentMethod The method of payment selected.
 * @param currency The currency selected.
 * @param orderId The order id.
 * @param userKey The user id.
 * @param allowedCards The cards that are allowed.
 */
-(void)setupWithAuthToken:(NSString *)authToken scheme:(UrlScheme)scheme amount:(float)amount paymentMethod:(PaymentMethod)method currencyCode:(Currency)currency andOrderId:(NSString *)orderId forUser:(NSString*)userKey allowedCards:(AUCreditCardType)allowedCards withBlockResult:(void(^)(BOOL result,id responseObject))blockResult withCompletion:(void (^)(void))completionBlock;

/* The language that the library is dipslayed in.*/
@property (nonatomic) PaymentLanguage language;
/* The theme that the library uses. */
@property (nonatomic) DisclaimerViewTheme theme;
/* The color of the user input fields descriptions. */
@property (nonatomic,strong) UIColor *colorAccents;
/* The color of the text when the user is typing something in. */
@property (nonatomic,strong) UIColor *colorTextInputs;
/* The background color of each section e.g. the card number, the expiration or the cvc.*/
@property (nonatomic,strong) UIColor *colorSectionBackground;
/* The background image that covers the entire screen.*/
@property (nonatomic,strong) UIImage *backgroundImage;
/* The background color for the entire screen.*/
@property (nonatomic,strong) UIColor *backgroundColor;


@end
