# Borgun


## Installation

Add the following to your Podfile
```ruby
pod 'Borgun', :git => 'https://bitbucket.org/stokkur/borgun-cocoapod-repo.git'
```

Run
```ruby
pod install
``` 

Add the following flag to your 'Other Linker Flags' section in your project target under Build Settings: 
```ruby
-lc++
```

Add: Pods/Borgun/LibBorgunPaymentLib.a to 'Link Binary With Libraries' in your project target under Build Phases if it is not there already.

Add the following frameworks to your project:
Accelerate, AudioToolBox, AVFoundation, CoreGraphics, CoreMedia, CoreVideo, MobileCoreServices, OpenGLES, QuartzCore, Security, CoreText.

Add: 'Pods/Borgun/Resources/ResourceBundle.bundle' to 'Copy Bundle Resources' in your project target under Build Phases

Add:

```ruby
<key>UIAppFonts</key>
    <array>
        <string>itc-officina-sans-lt-book.ttf</string>
        <string>itc-officina-sans-lt-bold.ttf</string>
    </array>
<key>NSCameraUsageDescription</key>
    <string>Borgun wants to access your camera to be able to scan your card.</string>
<key>NSPhotoLibraryUsageDescription</key>
    <string>Borgun wants to access the photo library to save receipts.</string>

``` 
To your project's Info.plist file.

Now you are good to go. Check the BorgunSampleProject located in this repository if you have any issues. 

Note: If you are using AFNetworking make sure to use the library through Cocoapods.

## Authors

Ivar (ivarhuni@stokkur.is), Gunnar (gunnar@stokkur.is)